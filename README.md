# Fixed Full Height Menu

The idea for this piece was to make it accessible to all kinds of screens at full width this is going to be my attempts (I don't consider bootstrap to work on large resolutions)

### Goals
  - Work on almost all Devices
  - Use Jade (That I haven't used before)
  - Use build system

### Version
0.1.0

### Tech

This uses a number of open source projects and tools to work properly:

* [Jade] - HTML preprocessor
* [SCSS] - CSS preprocessor
* [Jquery] - Javascript Library

* [PrePros] - Livereload and Compiler

### Installation

Have node and bower installed then run the following commands in the directory.

 - npm install
 - bower install
 - grunt

### Todos

 - Typography
 - Animations