module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      wiredep: {
          files: ['bower.json','bower_components/**/*'],
          tasks: ['wiredep']
      },
      styles: {
          files: ['app/css/**/*.scss'],
          tasks: ['sass']
      },
      jade: {
          files: ['app/**/*.jade', 'app/includes/**/*.jade'],
          tasks: ['jade','wiredep']
      },
      images: {
          files: ['app/Images'],
          tasks: ['copy:Images']
      },
    },
    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'build/css/main.css': 'app/css/master.scss'
        }
      }
    },
    jade: {
      debug: {
        options: {
          data: {
            expand: true,
            style: 'expanded'
          }
        },
        files: {
          "build/index.html": ["app/*.jade"]
        }
      }
    },
    copy: {
      Images: {
        files: [
          {expand: true, flatten: true, src: ['app/Images/**','app/Images/*'], dest: 'build/Images/', filter: 'isFile'},
        ],
      },
      js: {
        files: [
          {expand: true, flatten: true, src: ['app/js/*'], dest: 'build/js/', filter: 'isFile'},
        ],
      },
    },
    wiredep: {
      task: {
        src: [
          'build/index.html',
        ],
      }
    },
    browserSync: {
      dev: {
        bsFiles: {
            src : [
                'build/**/*','bower_components/*'
            ],
            routes: {
              "/bower_components": "/bower_components"
            }
        },
          options: {
            watchTask: true,
            server: './build',
          },

      }
    }

  });


  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-wiredep');

  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('compile', ['sass','jade']);
  grunt.registerTask('combuild', ['compile','copy']);
  grunt.registerTask('default', ['compile','copy','browserSync', 'watch', 'wiredep']);
};
